<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected array $tables = [
        'hybrid_files',
    ];

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->hybrid_files();
    }

    protected function hybrid_files()
    {
        Schema::create(
            'hybrid_files',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 64);
                $table->string('mime', 20);
                $table->integer('size');
                $table->timestamps();
            }
        );
    }
};
