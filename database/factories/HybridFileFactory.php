<?php

namespace Database\Factories\Smorken\HybridFile\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Smorken\HybridFile\Models\Eloquent\HybridFile;

class HybridFileFactory extends Factory
{
    protected $model = HybridFile::class;

    public function definition(): array
    {
        return [
            'name' => Str::random(16).'.png',
            'mime' => 'image/png',
            'size' => $this->faker->randomNumber(10),
        ];
    }
}
