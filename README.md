## Laravel 6 Hybrid File package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+
* [Composer](https://getcomposer.org/)

#### Use

The service provider should auto-register, if not

* add service provider to `config/app.php`

```
'providers' => [
...
  Smorken\HybridFile\ServiceProvider::class,
```
