<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/31/16
 * Time: 10:51 AM
 */

namespace Smorken\HybridFile\Models\Eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Support\Facades\App;
use Smorken\HybridFile\Contracts\Model;
use Smorken\HybridFile\Models\Builders\HybridFileBuilder;
use Smorken\Model\Eloquent;

#[\AllowDynamicProperties]
class HybridFile extends Eloquent implements Model
{
    /** HasBuilder<HybridFileBuilder<static>> */
    use HasBuilder;

    protected static array $options = [
        'disk' => 'local',
        'naming' => '%d.data',
        'provider' => 'filesystem',
        'allowed_types' => [
            'image/png' => '.png',
            'image/jpeg' => '.jpg',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/pdf' => '.pdf',
            'application/vnd.ms-powerpoint' => '.ppt',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation' => '.pptx',
            'text/plain' => '.txt',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx',
        ],
    ];

    protected static string $builder = HybridFileBuilder::class;

    protected $fillable = ['name', 'mime', 'size'];

    protected ?string $raw_data = null;

    public static function fromNameAndData(string $name, string $data): Model
    {
        // @phpstan-ignore new.static
        $i = new static(['name' => $name]);
        $i->setRawData($data);

        return $i;
    }

    public static function init(array $options): void
    {
        foreach ($options as $k => $v) {
            self::$options[$k] = $v;
        }
    }

    public function data(): Attribute
    {
        return Attribute::make(
            get: fn (): string => $this->getRawData()
        );
    }

    public function fileName(): Attribute
    {
        return Attribute::make(
            get: function (): string {
                $f = sprintf($this->getOption('naming', '%d.data'), $this->id);

                return sprintf('%d/%s', (int) ($this->id / 100), $f);
            }
        );
    }

    public function getOption(string $key, mixed $default = null): mixed
    {
        return self::$options[$key] ?? $default;
    }

    public function getRawData(): string
    {
        if ($this->raw_data === null) {
            $this->raw_data = '';
            $sp = $this->getStorageProvider();
            $filename = $this->fileName;
            if ($sp->exists($filename)) {
                $this->raw_data = base64_decode((string) $sp->get($filename));
            }
        }

        return $this->raw_data;
    }

    public function setRawData(string $data): self
    {
        $this->raw_data = $data;
        $this->attributes['size'] = strlen($data);
        $this->initMime($data);

        return $this;
    }

    public function getStorageProvider(): mixed
    {
        $p = $this->getOption('provider', 'filesystem');
        if (is_string($p)) {
            $p = App::make($p);
            self::$options['provider'] = $p;
        }
        if (! self::$options['provider']) {
            throw new \InvalidArgumentException('No provider set.');
        }

        return self::$options['provider']->disk($this->getOption('disk', 'local'));
    }

    protected function initMime(string $data): void
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $this->attributes['mime'] = finfo_buffer($finfo, $data);
        finfo_close($finfo);
    }

    public function newFromNameAndData(string $name, string $data): Model
    {
        $m = $this->newInstance(['name' => $name]);
        $m->setRawData($data);

        return $m;
    }
}
