<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\HybridFile\Models\Eloquent\HybridFile
 * @extends Builder<TModel>
 */
class HybridFileBuilder extends Builder
{
    use WithQueryStringFilter;

    public function defaultOrder(): EloquentBuilder
    {
        // @phpstan-ignore return.type
        return $this->orderBy('name');
    }
}
