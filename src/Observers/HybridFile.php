<?php

namespace Smorken\HybridFile\Observers;

use Smorken\HybridFile\Contracts\Model;

class HybridFile
{
    public function deleted(Model $model): bool
    {
        $sp = $model->getStorageProvider();
        // @phpstan-ignore property.notFound
        if ($sp->exists($model->fileName)) {
            // @phpstan-ignore property.notFound
            return $sp->delete($model->fileName);
        }

        return true;
    }

    public function saved(Model $model): bool
    {
        try {
            return $model->getStorageProvider()
                ->put($model->fileName, base64_encode($model->getRawData())); // @phpstan-ignore property.notFound
        } catch (\Throwable $e) {
            $this->deleteWithoutException($model);
            throw $e;
        }
    }

    protected function deleteWithoutException(Model $model): void
    {
        try {
            // @phpstan-ignore method.notFound
            $model->delete();
        } catch (\Throwable) {
            // no nothing
        }
    }
}
