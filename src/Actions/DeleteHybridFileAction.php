<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Actions;

use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\HybridFile\Contracts\Model;

class DeleteHybridFileAction extends EloquentDeleteAction implements \Smorken\HybridFile\Contracts\Actions\DeleteHybridFileAction
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
