<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\HybridFile\Contracts\Model;

class CreateHybridFileAction extends ActionWithEloquent implements \Smorken\HybridFile\Contracts\Actions\CreateHybridFileAction
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function __invoke(string $name, string $data): Model
    {
        // @phpstan-ignore method.notFound
        $m = $this->eloquentModel()->newFromNameAndData($name, $data);
        $m->save();

        return $m;
    }
}
