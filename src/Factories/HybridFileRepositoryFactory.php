<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Factories;

use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\HybridFile\Contracts\Model;
use Smorken\HybridFile\Contracts\Repositories\FilteredHybridFilesRepository;
use Smorken\HybridFile\Contracts\Repositories\FindHybridFileRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

class HybridFileRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindHybridFileRepository::class,
        'filtered' => FilteredHybridFilesRepository::class,
    ];

    public function emptyModel(): Model
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Support\Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): ?Model
    {
        return $this->handlerForFind($id, $throw);
    }
}
