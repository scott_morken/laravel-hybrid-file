<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Smorken\HybridFile\Contracts\Actions\CreateHybridFileAction;
use Smorken\HybridFile\Contracts\Actions\DeleteHybridFileAction;
use Smorken\HybridFile\Contracts\Model;

class HybridFileActionFactory extends ActionFactory
{
    protected array $handlers = [
        'delete' => DeleteHybridFileAction::class,
    ];

    public function create(string $name, string $data): Model
    {
        return $this->for(CreateHybridFileAction::class, ['name' => $name, 'data' => $data]);
    }
}
