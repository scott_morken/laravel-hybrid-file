<?php

namespace Smorken\HybridFile;

use Illuminate\Contracts\Foundation\Application;
use Smorken\HybridFile\Contracts\Model;
use Smorken\HybridFile\Validation\RuleProviders\HybridFileRules;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->registerModels();
        $this->registerActions();
        $this->registerRepositories();
        $options = $this->app['config']->get('sm-hybridfile.options', []);
        $this->initHybridFileModel($options);
        HybridFileRules::$allowed = $options['allowed_types'] ?? [];
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'sm-hybridfile');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('sm-hybridfile.php')], 'config');
    }

    protected function initHybridFileModel(array $options): void
    {
        $model = $this->app[Model::class];
        $modelClass = $model::class;
        $options['provider'] = $this->app['filesystem'];
        $modelClass::init($options);
        $modelClass::observe(\Smorken\HybridFile\Observers\HybridFile::class);
    }

    protected function registerActions(): void
    {
        foreach ($this->app['config']->get('sm-hybridfile.actions', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerModels(): void
    {
        foreach ($this->app['config']->get('sm-hybridfile.models', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerRepositories(): void
    {
        foreach ($this->app['config']->get('sm-hybridfile.repositories', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }
}
