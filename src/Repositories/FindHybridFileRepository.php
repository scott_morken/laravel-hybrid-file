<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\HybridFile\Contracts\Model;

class FindHybridFileRepository extends EloquentRetrieveRepository implements \Smorken\HybridFile\Contracts\Repositories\FindHybridFileRepository
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
