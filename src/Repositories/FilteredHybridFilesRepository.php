<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\HybridFile\Contracts\Model;

class FilteredHybridFilesRepository extends EloquentFilteredRepository implements \Smorken\HybridFile\Contracts\Repositories\FilteredHybridFilesRepository
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
