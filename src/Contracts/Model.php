<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/31/16
 * Time: 10:46 AM
 */

namespace Smorken\HybridFile\Contracts;

/**
 * Interface Model
 *
 *
 * @property int $id
 * @property string $mime
 * @property int $size
 * @property string $fileName
 * @property mixed $data
 */
interface Model extends \Smorken\Model\Contracts\Model
{
    public function getOption(string $key, mixed $default = null): mixed;

    public function getRawData(): string;

    public function getStorageProvider(): mixed;

    public function newFromNameAndData(string $name, string $data): self;

    public function setRawData(string $data): self;

    public static function fromNameAndData(string $name, string $data): self;

    public static function init(array $options): void;
}
