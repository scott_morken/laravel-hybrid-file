<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\HybridFile\Contracts\Model;

interface CreateHybridFileAction extends Action
{
    public function __invoke(string $name, string $data): Model;
}
