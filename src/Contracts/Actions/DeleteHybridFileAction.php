<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface DeleteHybridFileAction extends DeleteAction {}
