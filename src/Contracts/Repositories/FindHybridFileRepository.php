<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface FindHybridFileRepository extends RetrieveRepository {}
