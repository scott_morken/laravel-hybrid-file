<?php

declare(strict_types=1);

namespace Smorken\HybridFile\Validation\RuleProviders;

class HybridFileRules
{
    public static array $allowed = [
        'image/png' => '.png',
        'image/jpeg' => '.jpg',
        'application/msword' => '.doc',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
        'application/pdf' => '.pdf',
        'application/vnd.ms-powerpoint' => '.ppt',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => '.pptx',
        'text/plain' => '.txt',
        'application/vnd.ms-excel' => '.xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx',
    ];

    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required',
            'mime' => 'required|in:'.implode(',', self::$allowed),
            'size' => 'required|integer',
        ];
    }
}
