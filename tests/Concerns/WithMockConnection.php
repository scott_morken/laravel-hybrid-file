<?php

declare(strict_types=1);

namespace Smorken\Tests\HybridFile\Concerns;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Arr;
use Mockery as m;

trait WithMockConnection
{
    protected array $connections = [];

    protected \PDO|m\MockInterface|null $pdo = null;

    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getConnection(?string $className = null): ConnectionInterface
    {
        if (! $className) {
            return Arr::first($this->connections);
        }

        return $this->connections[$className];
    }

    protected function getMockConnection(
        string $connectionClass,
        array $config = []
    ): ConnectionInterface {
        $c = $this->connections[$connectionClass] ?? null;
        if ($c) {
            return $c;
        }
        $connection = m::mock(new $connectionClass($this->getPdo(), '', '', $config))->makePartial();
        $connection->enableQueryLog();
        $this->connections[$connectionClass] = $connection;

        return $connection;
    }

    protected function getPdo(): \PDO
    {
        if (! $this->pdo) {
            $this->pdo = m::mock(\PDO::class);
        }

        return $this->pdo;
    }

    protected function getPdoStatement(): \PDOStatement
    {
        if (! $this->statement) {
            $this->statement = m::mock(\PDOStatement::class);
            $this->statement->shouldReceive('setFetchMode');
        }

        return $this->statement;
    }

    protected function initConnectionResolver(
        string $name = 'db',
        string $connectionClass = MySqlConnection::class
    ): void {
        $cr = new ConnectionResolver([$name => $this->getMockConnection($connectionClass)]);
        $cr->setDefaultConnection($name);
        Model::setConnectionResolver($cr);
    }
}
