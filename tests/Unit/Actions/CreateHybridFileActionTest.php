<?php

declare(strict_types=1);

namespace Smorken\Tests\HybridFile\Unit\Actions;

use Carbon\Carbon;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\HybridFile\Actions\CreateHybridFileAction;
use Smorken\HybridFile\Models\Eloquent\HybridFile;
use Smorken\Tests\HybridFile\Concerns\WithMockConnection;

class CreateHybridFileActionTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_creates_a_hybrid_file(): void
    {
        Carbon::setTestNow('2023-12-14 15:00:00');
        $model = m::mock(new HybridFile)->makePartial();
        $this->getPdo()
            ->expects()
            ->prepare('insert into `hybrid_files` (`name`, `size`, `mime`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?)')
            ->andReturn($this->getPdoStatement());
        $binds = [
            [1, 'foo.txt', 2],
            [2, 11, 1],
            [3, 'text/plain', 2],
            [4, '2023-12-14 15:00:00', 2],
            [5, '2023-12-14 15:00:00', 2],
        ];
        foreach ($binds as $bind) {
            $this->getPdoStatement()->expects()->bindValue(...$bind);
        }
        $this->getPdoStatement()->expects()->execute();
        $this->getPdo()->expects()->lastInsertId('id')->andReturn(1);
        $sut = new CreateHybridFileAction($model);
        $m = $sut('foo.txt', file_get_contents(__DIR__.'/foo.txt'));
        $this->assertEquals([
            'name' => 'foo.txt',
            'size' => 11,
            'mime' => 'text/plain',
            'updated_at' => '2023-12-14T15:00:00.000000Z',
            'created_at' => '2023-12-14T15:00:00.000000Z',
            'id' => 1,
        ], $m->toArray());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Carbon::setTestNow();
    }
}
