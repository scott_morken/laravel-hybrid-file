<?php

namespace Smorken\Tests\HybridFile\Unit\Models;

use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\FilesystemManager;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\HybridFile\Contracts\Model;
use Smorken\HybridFile\Models\Eloquent\HybridFile;
use Smorken\Tests\HybridFile\Concerns\WithMockConnection;

class HybridFileTest extends TestCase
{
    use WithMockConnection;

    protected ?Filesystem $filesystem = null;

    protected ?FilesystemManager $filesystemManager = null;

    #[Test]
    public function it_attempts_to_delete_file(): void
    {
        $sut = $this->getSut();
        $data = file_get_contents(__DIR__.'/foo.txt');
        $sut->forceFill(['id' => 1]);
        $sut->setRawData($data);
        $sut->exists = true;
        $this->mockDelete('delete from `hybrid_files` where `id` = ?', [[1, 1, 1]]);
        $this->getFilesystem()->shouldReceive('exists')
            ->once()
            ->with('0/1.data')
            ->andReturn(true);
        $this->getFilesystem()->shouldReceive('delete')
            ->once()
            ->with('0/1.data')
            ->andReturn(true);
        $this->assertTrue($sut->delete());
    }

    #[Test]
    public function it_attempts_to_retrieve_file(): void
    {
        $sut = $this->getSut();
        $this->mockSelect(
            'select * from `hybrid_files` where `hybrid_files`.`id` = ? limit 1',
            [[1, 1, 1]],
            [['id' => 1, 'mime' => 'text/plain', 'size' => 1]]
        );
        $this->getFilesystem()->shouldReceive('exists')
            ->once()
            ->with('0/1.data')
            ->andReturn(true);
        $this->getFilesystem()->shouldReceive('get')
            ->once()
            ->with('0/1.data')
            ->andReturn('YXNkZmdoamtsOwo=');
        $m = $sut->find(1);
        $this->assertEquals("asdfghjkl;\n", $m->data);
    }

    #[Test]
    public function it_can_name_a_file(): void
    {
        HybridFile::unguard();
        $m = new HybridFile(['id' => 1]);
        $this->assertEquals('0/1.data', $m->fileName);
        $m = new HybridFile(['id' => 65]);
        $this->assertEquals('0/65.data', $m->fileName);
        $m = new HybridFile(['id' => 100]);
        $this->assertEquals('1/100.data', $m->fileName);
        $m = new HybridFile(['id' => 10033]);
        $this->assertEquals('100/10033.data', $m->fileName);
        HybridFile::reguard();
    }

    #[Test]
    public function it_can_save_a_file(): void
    {
        Carbon::setTestNow('2023-12-08 01:00:00');
        $sut = $this->getSut();
        $data = file_get_contents(__DIR__.'/foo.txt');
        $sut->setRawData($data);
        $this->mockInsert(
            'insert into `hybrid_files` (`size`, `mime`, `updated_at`, `created_at`) values (?, ?, ?, ?)',
            [
                [1, 11, 1],
                [2, 'text/plain', 2],
                [3, '2023-12-08 01:00:00', 2],
                [4, '2023-12-08 01:00:00', 2],
            ]
        );
        $this->getFilesystem()->shouldReceive('put')
            ->once()
            ->with('0/1.data', 'YXNkZmdoamtsOwo=')
            ->andReturn(true);
        $this->assertTrue($sut->save());
    }

    protected function getFilesystem(): Filesystem
    {
        if (! $this->filesystem) {
            $this->filesystem = m::mock(Filesystem::class);
        }

        return $this->filesystem;
    }

    protected function getFilesystemManager(): FilesystemManager
    {
        if (! $this->filesystemManager) {
            $fsm = m::mock(FilesystemManager::class);
            $fsm->shouldReceive('disk')
                ->andReturn($this->getFilesystem());
            $this->filesystemManager = $fsm;
        }

        return $this->filesystemManager;
    }

    protected function getSut(): Model
    {
        $config = [
            'provider' => $this->getFilesystemManager(),
        ];
        HybridFile::init($config);
        HybridFile::setEventDispatcher(
            new Dispatcher(
                m::mock(new Container)
                    ->makePartial()
            )
        );
        HybridFile::observe(\Smorken\HybridFile\Observers\HybridFile::class);

        return new HybridFile;
    }

    protected function mockDelete($expected_sql, $expected_params): void
    {
        $this->getPdo()->expects()->prepare($expected_sql)->andReturn($this->getPdoStatement());
        foreach ($expected_params as $v) {
            $this->getPdoStatement()->expects()->bindValue(...$v);
        }
        $this->getPdoStatement()->expects()->execute();
        $this->getPdoStatement()->expects()->rowCount()->andReturn(1);
    }

    protected function mockInsert($expected_sql, $expected_params): void
    {
        $this->getPdo()->expects()->prepare($expected_sql)->andReturn($this->getPdoStatement());
        foreach ($expected_params as $v) {
            $this->getPdoStatement()->expects()->bindValue(...$v);
        }
        $this->getPdoStatement()->expects()->execute();
        $this->getPdo()->expects()->lastInsertId('id')->andReturn(1);
    }

    protected function mockSelect($expected_sql, $expected_params, $return = []): void
    {
        $this->getPdo()->expects()->prepare($expected_sql)->andReturn($this->getPdoStatement());
        foreach ($expected_params as $v) {
            $this->getPdoStatement()->expects()->bindValue(...$v);
        }
        $this->getPdoStatement()->expects()->execute();
        $this->getPdoStatement()->expects()->fetchAll()->andReturns($return);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Carbon::setTestNow();
    }
}
