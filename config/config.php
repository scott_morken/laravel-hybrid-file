<?php

return [
    'options' => [
        'disk' => env('HF_STORAGE_DISK', 'local'),
        'naming' => env('HF_STORAGE_NAMING', '%d.data'),
        'allowed_types' => [
            'image/png' => '.png',
            'image/jpeg' => '.jpg',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/pdf' => '.pdf',
            'application/vnd.ms-powerpoint' => '.ppt',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation' => '.pptx',
            'text/plain' => '.txt',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx',
        ],
    ],
    'models' => [
        \Smorken\HybridFile\Contracts\Model::class => \Smorken\HybridFile\Models\Eloquent\HybridFile::class,
    ],
    'actions' => [
        \Smorken\HybridFile\Contracts\Actions\CreateHybridFileAction::class => \Smorken\HybridFile\Actions\CreateHybridFileAction::class,
        \Smorken\HybridFile\Contracts\Actions\DeleteHybridFileAction::class => \Smorken\HybridFile\Actions\DeleteHybridFileAction::class,
    ],
    'repositories' => [
        \Smorken\HybridFile\Contracts\Repositories\FilteredHybridFilesRepository::class => \Smorken\HybridFile\Repositories\FilteredHybridFilesRepository::class,
        \Smorken\HybridFile\Contracts\Repositories\FindHybridFileRepository::class => \Smorken\HybridFile\Repositories\FindHybridFileRepository::class,
    ],
];
